<div class="row">
	<div class="col-lg-12 col-sm-12 col-xs-12">
        <div class="widget">
            <div class="widget-header bordered-left bordered-darkorange">
                <span class="widget-caption"><?php echo $judul_page ?> </span>
            </div>
            <div class="widget-body bordered-left bordered-warning">

            	<?php 
            	$data_mhs = $this->db->get_where('mahasiswa',array('nim'=>$nim))->row();

            	 ?>
                
                <br><br>
                <div class="table-scrollable">
                <table align="center" class="table table-bordered">
				    <tr>
				        <td align="center" colspan="8" style="font-size: 16px;">
				        	<strong><u>KARTU RENCANA STUDI (KRS)</u></strong>
				        </td>
				    </tr>
				    <tr>
				    	<td colspan="8">&nbsp</td>
				    </tr>
				    <tr>
				        <td align="left" width="20%" colspan="2" ><strong>Nama Mahasiswa</strong></td>
				        <td align="left" width="30%" colspan="2"><strong>:</strong> <?php echo $data_mhs->nama ?></td>
				        <td align="left" width="20%" colspan="2" ><strong>NIM</strong></td>
				        <td align="left" colspan="2"><strong>:</strong> <?php echo $data_mhs->nim ?></td>
				    </tr>
				    <tr>
				        <td align="left" colspan="2"><strong>Program Studi</strong></td>
				        <td align="left" colspan="2"><strong>:</strong> <?php echo get_data('prodi','id_prodi',$data_mhs->id_prodi,'jenjang') ?> - <?php echo get_data('prodi','id_prodi',$data_mhs->id_prodi,'prodi') ?></td>
						        <td align="left" colspan="2"><strong>Periode</strong></td>
				        <td align="left" colspan="2"><strong>:</strong> <?php echo get_data('tahun_akademik','kode_tahun',$kode_semester,'keterangan') ?></td>
						    </tr>
						<tr>
								<td align="left" colspan="2"><strong>Semester</strong></td>
								<td align="left" colspan="2"><strong>:</strong> <?php echo get_semester($data_mhs->nim,tahun_akademik_aktif('kode_tahun')) ?></td>

								<td align="left" colspan="2"><strong>Disetujui Dosen PA</strong></td>
								<td align="left" colspan="2"><strong>:</strong> <?php 
								if (pengajuan_krs($data_mhs->nim)) {
									if (setuju_dosen_pa($data_mhs->nim)) {
										echo '<span class="label label-success">disetejui</span>';
									} else {
										if (ditolak_dosen_pa($data_mhs->nim)) {
											?>
											<span class="label label-danger">Di Tolak</span>
											<a data-toggle="modal" data-target="#mdl_alasan">lihat</a>

											<!-- Modal -->
											<div class="modal fade" id="mdl_alasan" role="dialog">
												<div class="modal-dialog">

												  <!-- Modal content-->
												  <div class="modal-content">
												    <div class="modal-header">
												      <button type="button" class="close" data-dismiss="modal">&times;</button>
												      <h4 class="modal-title">Alasan ditolak</h4>
												    </div>
												    <div class="modal-body">
												    	
												    	<?php 
												    	$this->db->where('nim', $nim);
								                		$this->db->where('kode_semester', $kode_semester);
								                		$this->db->order_by('created_at', 'desc');
								                		$alasan = $this->db->get('krs_ditolak')->row()->alasan_ditolak;
								                		echo $alasan;
												    	 ?>
												    	
												    </div>
												    <div class="modal-footer">
												    	
												     	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
												      </form>
												    </div>
												  </div>
												  
												</div>
											</div>
											<?php
										} else {
											echo '<span class="label label-warning">belum disetejui</span>';
										}
									}
									// echo $retVal = (setuju_dosen_pa($data_mhs->nim)) ? '<span class="label label-success">disetejui</span>' : '<span class="label label-warning">belum disetejui</span>' ; 
								} else {
									echo '<span class="label label-warning">belum diajukan, silahkan ajuka terlebih dahulu</span>';
								}
								
								?></td>
						</tr>
						<!-- <tr>
					        <td align="left" width="20%" colspan="2" ><strong>IPS Lalu</strong></td>
					        <td align="left" width="30%" colspan="2"><strong>:</strong> 
					        	<?php 
					        	$ips_lalu = ips($data_mhs->nim,cek_periode_lalu($kode_semester));
					        	echo number_format($ips_lalu,2).' ('.cek_periode_lalu($kode_semester).')' ?>
					        </td>
					        <td align="left" width="20%" colspan="2" ><strong>Batas SKS</strong></td>
					        <td align="left" colspan="2"><strong>:</strong> <?php echo batas_sks($ips_lalu) ?></td>
					    </tr> -->

					    <tr>
					        <td align="left" width="20%" colspan="2" ><strong>IPS Lalu</strong></td>
					        <td align="left" width="30%" colspan="2"><strong>:</strong> 
					        	<?php 
					        	$ips_lalu = ips($data_mhs->nim,cek_periode_lalu(tahun_akademik_aktif('kode_tahun'),$data_mhs->nim));

					        	if (get_semester($data_mhs->nim,tahun_akademik_aktif('kode_tahun')) == 1) {
					        		echo 'Belum Ada';
					        	} else {
					        		echo number_format($ips_lalu,2).' ('.cek_periode_lalu(tahun_akademik_aktif('kode_tahun'),$data_mhs->nim).')';
					        	}
					        	
					        	
					        	 ?>
					        </td>
					        <td align="left" width="20%" colspan="2" ><strong>Batas SKS</strong></td>
					        <td align="left" colspan="2"><strong>:</strong> <?php 
					        if (get_semester($data_mhs->nim,tahun_akademik_aktif('kode_tahun')) == 1) {
					        		echo '24';
					        	} else {
					        		echo batas_sks($ips_lalu);
					        	}
					         ?></td>
					    </tr>

				</table>
				<br><br>

                
	                <table class="table table-bordered table-hover table-striped">
	                    <thead>
	                        <tr role="row">
	                            <th rowspan="2" style="text-align: center; vertical-align: middle;">No.</th>
	                            <th rowspan="2" style="text-align: center; vertical-align: middle;">Kode MK</th>
	                            <th rowspan="2" style="text-align: center; vertical-align: middle;">Nama MK</th>
	                            <th rowspan="2" style="text-align: center; vertical-align: middle;">Dosen Pengajar</th>
	                            <th rowspan="2" style="text-align: center; vertical-align: middle;">SKS</th>
	                            <th rowspan="2" style="text-align: center; vertical-align: middle;">Kelas</th>
	                            <th colspan="3" style="text-align: center;">Jadwal Perkuliahan</th>
	                        </tr>
	                        <tr>
	                        	<th>Ruang</th>
	                        	<th>Hari</th>
	                        	<th>Waktu</th>
	                        </tr>
	                    </thead>
	                    <tbody>
	                    <?php 
	                    $no=1;
	                    $sks_total = 0;
	                    	$this->db->where('kode_semester', $kode_semester);
	                    	$this->db->where('nim', $nim);
	                    	foreach ($this->db->get('krs')->result() as $br): ?>
	                    		<tr>
	                    			<td><?php echo $no; ?></td>
		                    		<td><?php echo get_data('matakuliah','id_mk',$br->id_mk,'kode_mk') ?></td>
		                    		<td><?php echo get_data('matakuliah','id_mk',$br->id_mk,'nama_mk') ?></td>
		                    		<td><?php echo get_data('dosen','id_dosen',$br->id_dosen,'nama') ?></td>
		                    		<td><?php $sks = get_data('matakuliah','id_mk',$br->id_mk,'sks_total');
		                    			echo $sks;
		                    			$sks_total = $sks_total + $sks;
		                    		 ?></td>
		                    		<td><?php echo get_data('jadwal_kuliah','id_jadwal',$br->id_jadwal,'kelas') ?></td>
		                    		<td><?php echo get_data('jadwal_kuliah','id_jadwal',$br->id_jadwal,'ruang') ?></td>
		                    		<td><?php echo get_data('jadwal_kuliah','id_jadwal',$br->id_jadwal,'hari') ?></td>
		                    		<td><?php echo get_data('jadwal_kuliah','id_jadwal',$br->id_jadwal,'jam_mulai').' - '.get_data('jadwal_kuliah','id_jadwal',$br->id_jadwal,'jam_selesai')  ?></td>
		                    	</tr>
	                    	<?php $no++; endforeach ?>
	                    	<tr>
	                    		<td colspan="4"><b>Total SKS</b></td>
	                    		<td colspan="6"><?php echo $sks_total ?></td>
	                    	</tr>
	                        
	                    </tbody>
	                </table>
            	</div>

            	<div>
            		<br>
            		<button class="btn btn-primary" data-toggle="modal" data-target="#myModal" id="btn_tolak"><i class="fa fa-plus"></i> Tolak KRS</button>
            		<a onclick="javasciprt: return confirm('Yakin akan setujui KRS mahasiswa ini ?')" href="dosen_pa/setujui?nim=<?php echo $nim ?>&kode_semester=<?php echo $kode_semester ?>&id_prodi=" class="btn btn-info">Setujui</a>


            		<!-- Modal -->
					<div class="modal fade" id="myModal" role="dialog">
						<div class="modal-dialog">

						  <!-- Modal content-->
						  <div class="modal-content">
						    <div class="modal-header">
						      <button type="button" class="close" data-dismiss="modal">&times;</button>
						      <h4 class="modal-title">Form Penolakan KRS</h4>
						    </div>
						    <div class="modal-body">
						    	<form action="dosen_pa/aksi_simpan_penolakan" method="POST">
						    		<div class="form-group">
						    			<label>Pesan</label>
						    			<input type="hidden" name="nim" value="<?php echo $nim ?>">
						    			<input type="hidden" name="kode_semester" value="<?php echo $kode_semester ?>">
						    			<input type="hidden" name="param" value="<?php echo param_get() ?>">
						    			<textarea class="form-control textarea_editor" name="alasan_ditolak"rows="5"></textarea>
						    		</div>
						    	
						    </div>
						    <div class="modal-footer">
						    	<button class="btn btn-primary" type="submit">Simpan</button>
						     	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						      </form>
						    </div>
						  </div>
						  
						</div>
					</div>
					  

            		<br><br>

	            	<div class="table-scrollable">
	                <table align="center" class="table table-bordered">
	                	<thead>
	                		<tr>
	                			<th>Date</th>
	                			<th>Pesan</th>
	                			<th>Aksi</th>
	                		</tr>
	                	</thead>
	                	<tbody>
	                		<?php 
	                		$this->db->where('nim', $nim);
	                		$this->db->where('kode_semester', $kode_semester);
	                		$this->db->order_by('created_at', 'desc');
	                		foreach ($this->db->get('krs_ditolak')->result() as $rw): ?>
	                		<tr>
	                			<td><?php echo $rw->created_at ?></td>
	                			<td><?php echo $rw->alasan_ditolak ?></td>
	                			<td>
	                				<a href="dosen_pa/hapus_riwayat_tolak/<?php echo $rw->id.'?'.param_get() ?>" onclick="javasciprt: return confirm('Apakah kamu yakin akan menghapus data ini?')" class="label label-danger">Hapus</a>
	                			</td>
	                		</tr>
	                		<?php endforeach ?>
	                	</tbody>
	                </table>
            	</div>

            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="assets/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
    tinymce.init({
        selector: ".textarea_editor",
        height: "500",
        plugins: [
             "advlist autolink link image lists charmap print preview hr anchor pagebreak fullscreen",
             "searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking",
             "table contextmenu directionality emoticons paste textcolor code"
       ],
       toolbar1: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | styleselect",
       image_advtab: true ,
       
      
   });

</script>